//Swap position of companies with id 93 and id 92.
function swapThePositionOfIds(data, id1, id2, callback) {
  try {
    let employee1 = undefined;
    let employee2 = undefined;
    let employees = data.employees;
    if (!Array.isArray(employees)) {
      throw new Error("items is not an array");
    }

    for (let employee of employees) {
      if (employees.id === id1) {
        employee1 = employee;
      }
      if (employees.id === id2) {
        employee2 = employee;
      }

      if (employee1 && employee2) {
        break;
      }
    }

    if (employee1 && employee2) {
      let temp = employee1;
      employee1 = employee2;
      employee2 = temp;
    }
    callback(null, employees);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = swapThePositionOfIds;
