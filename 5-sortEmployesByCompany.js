//sort data based on company name. If the company name is same, use id as the secondary sort metric.

const data = require("./data.json");

function getSortDataByCompanies(data, callback) {
  try {
    const newData = JSON.parse(JSON.stringify(data));
    const employees = newData.employees;
    if (!Array.isArray(employees)) {
      throw new Error("items is not an array");
    }
    employees.sort((a, b) => {
      if (a.company < b.company) {
        return -1;
      }
      if (a.company > b.company) {
        return 1;
      }
      return a.id - b.id;
    });
    callback(null, employees);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = getSortDataByCompanies;
