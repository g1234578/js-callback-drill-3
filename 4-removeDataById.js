//4. Remove entry with id 2.

function removeDataOfGivenData(data, id, callback) {
  try {
    const employees = data.employees;

    if (!Array.isArray(employees)) {
      throw new Error("items is not an array");
    }

    const dataWithOutGivenId = employees.filter((key) => key.id !== id);
    //console.log(dataWithOutGivenId);
    callback(null, dataWithOutGivenId);
  } catch (error) {
    console.log(error);
  }
}

module.exports = removeDataOfGivenData;
