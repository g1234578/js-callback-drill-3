const fs = require("fs");
const path = require("path");

function writeFileData(filename, data) {
    let content = JSON.stringify(data);
    fs.writeFile(filename, content, (err) => {
        if (err) {
          console.log("Error occured while writing file");
          console.error(err);
        } else {
          // file written successfully
          console.log("File written successfully");
        }
      });
}

module.exports = writeFileData;
