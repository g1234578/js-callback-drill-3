const givendata = require('../data.json');
const fs = require('fs')
const getDataFromGivenID = require("../1-getDataById");
const getGroupFromGivenCompanies = require('../2-getEmployesGroupByCompanies');
const getGroupFromAGivenCompany = require('../3-employesDataByGivenCompany');
const removeDataOfGivenData = require('../4-removeDataById');
const getSortDataByCompanies = require('../5-sortEmployesByCompany');
const swapThePositionOfIds = require('../6-swapEmployesByID');
const getEmployeeWhoseIDIsEven = require('../7-employeeByEvenId');
const writeFileData = require('./writeFile');


function performActions(){

    fs.mkdir("./output", { recursive: true }, (err) => {
        if (err) {
          console.log("Error occured while creating folder");
          console.error(err);
        } else {
          console.log("Folder created successfully");
        }

        getDataFromGivenID(givendata,[2, 13, 23],(err,data)=>{
            if (err) {
                console.error(err);
              } else {
                writeFileData("output/1-employees-by-id.json", data);
                getGroupFromGivenCompanies(givendata, (err,data)=>{
                    if (err) {
                        console.error(err);
                      } else {
                        writeFileData("output/2-employees-groupby-companies.json", data);

                        getGroupFromAGivenCompany(givendata,(err,data)=>{
                            if (err) {
                                console.error(err);
                              } else {
                                writeFileData("output/3-employees-by-givenCompany.json", data);

                                removeDataOfGivenData(givendata,2,(err,data)=>{
                                    if (err) {
                                        console.error(err);
                                      } else {
                                        writeFileData("output/4-remove-employee-by-id.json", data);
                                        getSortDataByCompanies(givendata,(err,data)=>{
                                            if (err) {
                                                console.error(err);
                                              } else {
                                                writeFileData("output/5-sort-employee-by-companies.json", data);
                                                swapThePositionOfIds(givendata,92,93,(err,data)=>{
                                                    if (err) {
                                                        console.error(err);
                                                      } else {
                                                        writeFileData("output/6-swap-employee-by-ids.json", data);
                                                        getEmployeeWhoseIDIsEven(givendata,(err,data)=>{
                                                            if (err) {
                                                                console.error(err);
                                                              } else {
                                                                writeFileData("output/7-employee-by-evenID.json", data);
                                                              }
                                                        })
                                                      }
                                                })
                                              }
                                        })
                                      }
                                })
                              }
                        })
                      }
                })
              }
    })

})
}

performActions();

