const fs = require("fs");
const path = require("path");
const data = require("./1-employees-callbacks.cjs");

function convertDataToJson() {
  fs.writeFile("data.json", JSON.stringify(data), (err) => {
    if (err) console.log(err);
    else {
      console.log("File written successfully\n");
      console.log("The written has the following contents:");
      console.log(fs.readFileSync("data.json", "utf8"));
    }
  });
}

module.exports = convertDataToJson;
