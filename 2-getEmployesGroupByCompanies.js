// 2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

const data = require("./data.json");

function getGroupFromGivenCompanies(data, callback) {
  try {
    const employees = data.employees;

    if (!Array.isArray(employees)) {
      throw new Error("items is not an array");
    }

    const companiesData = employees.reduce((accumaltor, employeesData) => {
      let company = employeesData.company;
      let name = employeesData.name;

      if (accumaltor[company] === undefined) {
        accumaltor[company] = [];
      }

      accumaltor[company].push(name);

      return accumaltor;
    }, {});
    callback(null, companiesData);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = getGroupFromGivenCompanies;
