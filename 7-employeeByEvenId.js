//For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
const currentDate = new Date();
const data = require("./data.json");

function getEmployeeWhoseIDIsEven(data, callback) {
  try {
    const employees = data.employees;
    if (!Array.isArray(employees)) {
      throw new Error("items is not an array");
    }

    employees.forEach((employee) => {
      if (employee.id % 2 === 0) {
        employee.birthday = `${currentDate.getFullYear()}-${String(
          currentDate.getMonth() + 1
        ).padStart(2, "0")}-${String(currentDate.getDate()).padStart(2, "0")}`;
      }
    });

    callback(null, employees);

    //console.log(employees);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = getEmployeeWhoseIDIsEven;
