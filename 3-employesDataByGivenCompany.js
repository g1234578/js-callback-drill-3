//3. Get all data for company Powerpuff Brigade
function getGroupFromAGivenCompany(data, callback) {
  try {
    const employees = data.employees;

    // Ensure items is an array
    if (!Array.isArray(employees)) {
      throw new Error("items is not an array");
    }

    const companiesData = employees.filter(
      (key) => key.company === "Powerpuff Brigade"
    );
    callback(null, companiesData);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = getGroupFromAGivenCompany;
