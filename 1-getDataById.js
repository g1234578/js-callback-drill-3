// 1. Retrieve data for ids : [2, 13, 23].
function getDataFromGivenID(data, ids, callback) {
  try {
    const items = data.employees;

    if (!Array.isArray(items)) {
      throw new Error("items is not an array");
    }

    const idsData = items.filter((item) => ids.includes(item.id));

    callback(null, idsData);
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = getDataFromGivenID;
